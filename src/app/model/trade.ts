enum TradeType{
  BUY = 'BUY',
  SELL = 'SELL'
}

enum TradeState{
  CREATED = 'CREATED',
  PROCESSING = 'PROCESSING',
  FILLED = 'FILLED',
  REJECTED = 'REJECTED'
}

export class Trade{

  public state: TradeState;
  public date: Date;
  public totalPrice: number;
  public unitPrice: number;
  public userId: string;
  public ticker: string;
  public compandName: string;
  public type: TradeType;
  public quantity: number;

}
