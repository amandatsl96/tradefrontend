import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateTradeComponent} from './component/create-trade/create-trade.component';

const routes: Routes = [
  {path: 'createTrade', component: CreateTradeComponent},
  { path: '', redirectTo: 'createTrade', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
