import { Component, OnInit } from '@angular/core';
import {TradeService} from '../../service/trade.service';
import {Trade} from '../../model/trade';

@Component({
  selector: 'app-create-trade',
  templateUrl: './create-trade.component.html',
  styleUrls: ['./create-trade.component.css']
})
export class CreateTradeComponent implements OnInit {

  userTrade: Trade = new Trade();
  submitted = false;

  constructor(private tradeService: TradeService) { }

  ngOnInit(): void {
  }

  newTrade(): void{
    this.submitted = false;
    this.userTrade = new Trade();
  }

  save() {
    this.tradeService.createTrade(this.userTrade)
      .subscribe(data => console.log(data), error => console.log(error));
    this.userTrade = new Trade();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }


}
