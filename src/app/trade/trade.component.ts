import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GetTradesService } from '../get-trades/get-trades.service';
import { Observable, BehaviorSubject, fromEvent, pipe } from 'rxjs';
import { Trade } from '../model/trade'
import { FetcherService } from '../price-fetcher/fetcher.service';
import { TradeService } from '../service/trade.service'
import { MatPaginator } from '@angular/material/paginator';
import { MatSpinner } from '@angular/material/progress-spinner'
import { MatInput } from '@angular/material/input'
import { Quote } from '../price-fetcher/quote';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})

export class TradeComponent {

  public trades$: Observable<Array<Trade>>;
  isLoading$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  displayedColumns: string[] = ['ticker', 'companyName', 'bought', 'sold', 'balance', 'price'];
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatInput) input: ElementRef;
  
  constructor(private getTradesService : GetTradesService, private fetcher : FetcherService) {}

  getPrice(ticker : string) : Observable<Quote> {
    return this.fetcher.fetch(ticker);
  }

  ngOnInit(): void {
    this.trades$ = this.getTradesService.getHistory();
  }

}
